FROM python:3-alpine
 
WORKDIR /usr/src/app
 
COPY requirements.txt ./
 
RUN apk update && \
 apk add chromium chromium-chromedriver && \
 python3 -m pip install -r requirements.txt --no-cache-dir
 
 COPY . .
 
 CMD ["behave"]
from pages.sort_list_page import SortListPage
from hamcrest import assert_that, is_


@given(u'I am on Sort List Page')
def step_impl(context):
    context.page_object = SortListPage(context.driver)
    context.page_object.open_page(context.base_url)


@when(u'I sort the elements of the list')
def step_impl(context):
    for index in range(6):
        context.page_object.drag_and_drop_item(str(index), str(index+1))


@then(u'I verify that the list is sorted')
def step_impl(context):
    for index in range(6):
        assert_that(context.page_object.get_list_item_text(str(index + 1)), is_("Item " + str(index)))

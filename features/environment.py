from os.path import dirname, realpath
from selenium import webdriver


def before_all(context):
    context.base_url = context.config.userdata['BASE_URL']


def before_scenario(context, scenario):
    if context.config.userdata['BROWSER'] == "headless":
        options = webdriver.ChromeOptions()
        options.add_argument('--no-sandbox')
        options.add_argument('--headless')
        options.add_argument('--disable-gpu')
        context.driver = webdriver.Chrome(chrome_options=options)
    else:
        context.driver = webdriver.Chrome(executable_path=dirname(realpath(__file__)) + "/resources/chromedriver")
    context.driver.implicitly_wait(30)
    context.driver.maximize_window()


def after_scenario(context, scenario):
    context.driver.close()

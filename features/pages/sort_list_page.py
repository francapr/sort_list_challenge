from selenium.webdriver.common.by import By
from pages.base_page import BasePage

class SortListPage(BasePage):
    ITEM_BY_TEXT = (By.XPATH, "//*[text()='Item {}']")
    ITEM_BY_POSITION = (By.CSS_SELECTOR, "ul > li:nth-child({})")


    def open_page(self, url):
        super().open_url(url)


    def get_list_item_text(self, position):
        item_by_position = (self.ITEM_BY_POSITION[0], self.ITEM_BY_POSITION[1].replace("{}", position))
        return super().find(item_by_position).text


    def drag_and_drop_item(self, item, position):
        item_by_text = (self.ITEM_BY_TEXT[0], self.ITEM_BY_TEXT[1].replace("{}", item))
        item_by_position = (self.ITEM_BY_POSITION[0], self.ITEM_BY_POSITION[1].replace("{}", position))
        element = super().find(item_by_text)
        target = super().find(item_by_position)
        super().drag_and_drop(element, target)
